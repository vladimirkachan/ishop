﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace IShop.Filters
{
    public class ExtendedActionFilterAttribute : ActionFilterAttribute
    {
        DateTime start;

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            base.OnActionExecuting(actionContext);
            start = DateTime.Now;
        }
        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            base.OnActionExecuted(actionExecutedContext);
            var end = DateTime.Now;
            actionExecutedContext.Response.Headers.Add("Start-time", start.ToLongTimeString());
            actionExecutedContext.Response.Headers.Add("End-time", end.ToLongTimeString());
        }
    }
}
