﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace IShop.Filters
{
    public class ShopExceptionFilterAttribute : Attribute, IExceptionFilter
    {
        public bool AllowMultiple => false;
        public async Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            var ex = actionExecutedContext.Exception;
            if (ex is IndexOutOfRangeException)
                actionExecutedContext.Response = actionExecutedContext.Request.CreateErrorResponse(System.Net.HttpStatusCode.BadRequest,
                                                                  ex.Message);
        }
    }
}
