﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace IShop.Filters
{
    public class ShopAuthorizationFilterAttribute : Attribute, IAuthorizationFilter
    {
        public bool AllowMultiple => false;
        public Task<HttpResponseMessage> ExecuteAuthorizationFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken,
                                                                         Func<Task<HttpResponseMessage>> continuation)
        {
            return Task.FromResult(actionContext.Request.CreateResponse(HttpStatusCode.NotFound));
        }
    }
}
