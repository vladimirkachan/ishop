﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace IShop.Filters
{
    public class SimpleActionFilterAttribute : Attribute, IActionFilter
    {
        public bool AllowMultiple => false;
        public async Task<HttpResponseMessage> ExecuteActionFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            var result = await continuation();
            result.Headers.Add("IShop", DateTime.Now.ToString());
            return result;
        }
    }
}
