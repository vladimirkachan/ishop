﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Security.Principal;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Filters;
using System.Web.Http.Results;

namespace IShop.Filters
{
    public class ShopAuthenticationFilterAttribute : Attribute, IAuthenticationFilter
    {
        public bool AllowMultiple => false;
        public Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            context.Principal = null;
            var authentication = context.Request.Headers.Authorization;
            if (authentication != null && authentication.Scheme == "Basic")
            {
                var data = Encoding.ASCII.GetString(Convert.FromBase64String(authentication.Parameter)).Split(':');
                var roles = new[] {"user"};
                context.Principal = new GenericPrincipal(new GenericIdentity(data[0]), roles);
            }
            if (context.Principal == null) context.ErrorResult = new UnauthorizedResult(new[] {new AuthenticationHeaderValue("Basic")}, context.Request);
            return Task.FromResult<object>(null);
        }
        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.FromResult<object>(null);
        }
    }
}
