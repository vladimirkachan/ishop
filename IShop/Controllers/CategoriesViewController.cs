﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using IShop.Domain.Models;

namespace IShop.Controllers
{
    public class CategoriesViewController : Controller
    {
        // GET: CategoriesView
        public async Task<ActionResult> Index()
        {
            return View(await GetData());
        }

        async Task<List<Category>> GetData()
        {
            var client = new HttpClient {BaseAddress = new Uri("https://localhost:44375/")};
            client.DefaultRequestHeaders.Accept.Clear();
            client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));
            var response = await client.GetAsync("api/categories");
            List<Category> categories = null;
            if (response.IsSuccessStatusCode) categories = await response.Content.ReadAsAsync<List<Category>>();
            return categories;
        }
    }
}