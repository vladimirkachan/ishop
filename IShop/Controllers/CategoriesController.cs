﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IShop.BusinessLogic.Services;
using IShop.Domain.Models;
using IShop.Filters;

namespace IShop.Controllers
{
    [RoutePrefix("api/categories")]
    public class CategoriesController : ApiController
    {
        ICategoryService categoryService = new CategoryService();

        //[ShopAuthorizationFilter]
        //[SimpleActionFilter]
        //[ExtendedActionFilter]
        //[ShopExceptionFilter]
        //[Authorize]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            return Ok(categoryService.GetAll());
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            var category = categoryService.Get(id);
            if (category == null) return NotFound();
            return Ok(category);
        }

        [HttpPost]
        public IHttpActionResult Add([FromBody] Category category)
        {
            if (string.IsNullOrWhiteSpace(category.Name))
                return Ok("Can't be empty");
            categoryService.Add(category);
            return Ok();
        }

        [HttpPut]
        public IHttpActionResult Update([FromBody] Category category)
        {
            categoryService.Update(category);
            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            categoryService.Delete(id);
            return Ok();
        }

        [HttpGet]
        [Route("search")]
        public IHttpActionResult Search(string name)
        {
            return Ok(categoryService.GetAll().Where(c => c.Name.Contains(name.ToLower())).ToList());
        }
    }
}
