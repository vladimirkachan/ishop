﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IShop.BusinessLogic.Services;
using IShop.Domain.Models;

namespace IShop.Controllers
{
    public class ProductsController : ApiController
    {
        IProductService productService = new ProductService();

        [OverrideAuthentication]
        [HttpGet]
        public IHttpActionResult GetAll()
        {
            return Ok(productService.GetAll());
        }

        [HttpGet]
        public IHttpActionResult Get(int id)
        {
            return Ok(productService.Get(id));
        }

        [HttpPost]
        public HttpResponseMessage Add([FromBody] Product product)
         {
            if (!ModelState.IsReadOnly) return new HttpResponseMessage
            {
                Content = new StringContent("Name can't be empty"),
                StatusCode = HttpStatusCode.BadRequest
            };
            productService.Add(product);
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpPut]
        public IHttpActionResult Update([FromBody] Product product)
        {
            productService.Update(product);
            return Ok();
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            productService.Delete(id);
            return Ok();
        }
    }
}
