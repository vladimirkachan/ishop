﻿using IShop.Domain.Models;
using System.Collections.Generic;

namespace IShop.BusinessLogic.Services
{
    public interface ICategoryService
    {
        void Add(Category category);

        void Update(Category category);

        void Delete(int id);

        List<Category> GetAll();

        Category Get(int id);
    }
}
