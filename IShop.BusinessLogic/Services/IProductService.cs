﻿using IShop.Domain.Models;
using System.Collections.Generic;

namespace IShop.BusinessLogic.Services
{
    public interface IProductService
    {
        void Add(Product category);

        void Update(Product category);

        void Delete(int id);

        List<Product> GetAll();

        Product Get(int id);
    }
}
